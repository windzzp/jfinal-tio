package com.seven.controller;

import com.alibaba.fastjson.JSON;
import com.jfinal.core.Controller;
import com.seven.client.HelloClientStarter;
import com.seven.common.HelloPacket;
import com.seven.common.User;
import com.seven.server.HelloServerStarter;
import org.tio.core.Tio;

import java.io.UnsupportedEncodingException;

public class HelloTioController extends Controller {

    /**
     * 发送消息去客户端
     */
    public void helloserver() throws UnsupportedEncodingException {

        User user = new User();
        user.setId("1");
        user.setMsg("hello tio");
        user.setName("seven");

        String body = JSON.toJSONString(user);

        HelloPacket packet = new HelloPacket();
        packet.setBody(body.getBytes(HelloPacket.CHARSET));
        Tio.send(HelloClientStarter.clientChannelContext,packet);
        renderText("success");
    }



    /**
     * 发送消息去客户端
     */
    public void helloclient() throws UnsupportedEncodingException {
        HelloPacket packet = new HelloPacket();
        packet.setBody("hello client,i amd server".getBytes(HelloPacket.CHARSET));
        Tio.sendToUser(HelloServerStarter.serverGroupContext,"1",packet);
        renderText("success");
    }
}
